type RouteHandler = (...args: any[]) => void;

class Router {
  private routes: { [key: string]: RouteHandler } = {};

  add(route: string, handler: RouteHandler): void {
    this.routes[route] = handler;
  }

  call(route: string, ...args: any[]): void {
    for (const key in this.routes) {
      if (this.match(route, key)) {
        const handler = this.routes[key];
        if (typeof handler === "function") {
          handler(...args);
          return;
        } else {
          throw new Error(`Handler for route ${key} is not a function`);
        }
      }
    }

    throw new Error(`Route ${route} not found`);
  }

  private match(route: string, key: string): boolean {
    const routeParts = route.split("-");
    const keyParts = key.split("-");

    if (routeParts.length !== keyParts.length) {
      return false;
    }

    for (let i = 0; i < routeParts.length; i++) {
      if (routeParts[i] !== "*" && routeParts[i] !== keyParts[i]) {
        return false;
      }
    }

    return true;
  }
}

export { Router };
