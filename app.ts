import { Router } from "./router";

const router = new Router();

router.add("add-book", (a, b, c) => {
  console.log("Adding book with parameters: ", a, b, c);
});

router.add("add-movie", (a, b, c) => {
  console.log("Adding movie with parameters: ", a, b, c);
});

router.call("add-book", "BookTitle", "Author", 2024);
router.call("add-movie", "MovieTitle", "Director", 2024);
router.call("add-*", "Any", "Handler", "Arguments");
