import { Router } from "./router";

describe("Router", () => {
  let router;

  beforeEach(() => {
    router = new Router();
  });

  describe("add", () => {
    test("should add a new route", () => {
      const mockHandler = jest.fn();
      router.add("add-book", mockHandler);
      expect(router.routes["add-book"]).toBe(mockHandler);
    });

    test("should throw error when adding route with non-function handler", () => {
      expect(() => {
        router.add("invalid-route", "not a function");
      }).toThrow("Handler for route invalid-route is not a function");
    });
  });

  describe("call", () => {
    test("should call a route with correct arguments", () => {
      const mockHandler = jest.fn();
      router.add("add-book", mockHandler);
      router.call("add-book", "BookTitle", "Author", 2024);
      expect(mockHandler).toHaveBeenCalledWith("BookTitle", "Author", 2024);
    });

    test("should throw error when route not found", () => {
      expect(() => {
        router.call("non-existing-route");
      }).toThrow("Route non-existing-route not found");
    });

    test("should call route using wildcard", () => {
      const mockHandler = jest.fn();
      router.add("add-*", mockHandler);
      router.call("add-movie", "MovieTitle", "Director", 2024);
      expect(mockHandler).toHaveBeenCalledWith("MovieTitle", "Director", 2024);
    });
  });

  describe("match", () => {
    test("should match routes correctly", () => {
      expect(router.match("add-book", "add-book")).toBe(true);
      expect(router.match("add-*", "add-movie")).toBe(true);
      expect(router.match("add-*", "update-movie")).toBe(false);
      expect(router.match("add-*", "add-movie-extra")).toBe(false);
    });
  });
});
